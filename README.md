# README 

This repository contains some material for RailsGirls Tampere, mostly related to
Ruby.

## Quick tutorial

* [Basics, part I](https://bitbucket.org/edvardm/railsgirls-ruby/src/master/src/basics.md?at=master&fileviewer=file-view-default)
* [Basics, part II](https://bitbucket.org/edvardm/railsgirls-ruby/src/master/src/intermediate.md?at=master&fileviewer=file-view-default)

It is recommended to install the tool called 'pry' so that working in interactive Ruby 
prompt is easier. You can do it by just running

TBD: pry install script
